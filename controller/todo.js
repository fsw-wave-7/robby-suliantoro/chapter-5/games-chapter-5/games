const fs = require('fs')
const { successResponse } = require('../helper/response')

class Todo {
    constructor () {
    this.todo = []
    const readTodo = fs.readFileSync('./data/data.json', 'utf-8')
    if (readTodo == ""){
        this.parsedTodo = this.todo
    } else {
        this.parsedTodo = JSON.parse(readTodo)
    }
    }

    getTodo = (req, res) => {
        successResponse(res, 200, this.parsedTodo, {total: this.parsedTodo.length})
    }

    getDetailedTodo = (req, res) => {
        const index = req.params.index
        successResponse(res, 200, this.parsedTodo[index])
    
    }
    insertTodo = (req, res) => {
        const body = req.body
        const param = {
            "name" : body.name,
            "isDone": body.isDone
        }

        this.parsedTodo.push(param)
        fs.writeFileSync('./data/data.json', JSON.stringify(this.parsedTodo))
        successResponse(res, 201, param)

    }

    updateTodo = (req, res) => {
        const index = req.params.index
        const body = req.body
        
        if (this.parsedTodo[index]) {
        this.parsedTodo[index].name = body.name
        this.parsedTodo[index].isDone = body.isDone

        fs.writeFileSync("./data/data.json", JSON.stringify(this.parsedTodo))
        successResponse(res, 200, this.parsedTodo[index])
        }else {
            successResponse(res, 422, null)
        }
    }

    deleteTodo = (req, res) => {

        const index = req.params.index
        this.parsedTodo.splice(index, 1)

        successResponse(res, 200, null)
    }
}

module.exports = Todo