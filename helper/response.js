const messages = {
    "200": "sukses",
    "201": "User berhasil disimpan",
    "422": "Gagal Memproses"
}

function successResponse( res,code,data,meta = {}) {
    res.status(code).json({
        data: data,
        meta: {
            code: code,
            messages: messages[code.toString()],
            ...meta
        }
    })
}

module.exports = { successResponse}