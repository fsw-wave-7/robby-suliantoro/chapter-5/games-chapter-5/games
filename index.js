const express = require('express')
const bodyParser = require('body-parser')
const fs = require('fs')

const app = express()
const port = 5000

const jsonParser = bodyParser.json()
app.use(jsonParser)

const Todo = require('./controller/todo')

const todo = new Todo


app.get("/", (req, res) => {
    res.json({
        "hello" : "world"
    })
})

app.get("/todo", todo.getTodo)
app.get("/todo/:index", todo.getDetailedTodo)
app.post("/todo", todo.insertTodo)
app.put("/done/:index", todo.updateTodo)
app.delete("/todo/:index",todo.deleteTodo)


app.listen(port, () => {
    console.log("server tugas kelompok jalan")
})